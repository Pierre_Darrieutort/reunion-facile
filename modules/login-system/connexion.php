<?php

require_once('../header.php');
require_once('../../config/db.php');

if(isset($_SESSION['id']) && $_SESSION['id'] > 0) {
    header("Location: ../../index.php");
}
else if(isset($_POST['connexionButton'])) {
    $pseudoConnect = htmlspecialchars($_POST['pseudoForm']);
    $mdpConnect = htmlspecialchars($_POST['passForm']);
    $userSearch = $bdd -> prepare('SELECT id, motdepasse FROM membres WHERE pseudo = ?');
    $userSearch -> execute(array($pseudoConnect));
    $verifyUser = $userSearch -> fetch();
    $mdpConnectSecure = password_verify($mdpConnect, $verifyUser['motdepasse']);
    
    if(!empty($pseudoConnect) && !empty($mdpConnect)) {
        $uniqueUser = $userSearch -> rowCount();
        if($uniqueUser == 1 && $mdpConnectSecure == true) {
            $_SESSION['id'] = $verifyUser['id'];
            $_SESSION['pseudo'] = $pseudoConnect;
        } else {
            $erreur = "Mauvais identifiant ou mot de passe";
        }
    } else {
        $erreur = "Tous les champs doivent être complétés";
    }
}

?>



<div class="container">
    <form id="formConnexion" method="POST">
        <div class="form-group">
            <label for="pseudoForm">Votre Pseudo</label>
            <input type="text" class="form-control" name="pseudoForm" placeholder="Votre pseudo">
        </div>
        <div class="form-group">
            <label for="passForm">Votre mot de passe</label>
            <input type="password" class="form-control" name="passForm" placeholder="Votre mot de passe">
        </div>
        <button type="submit" name="connexionButton" class="btn btn-primary">S'inscrire</button>
    </form>
</div>


<?php
    if(isset($erreur)) {
    echo '<font color="red">'.$erreur."</font>";
    }
    if(isset($success)) {
    echo '<font color="green">'.$success."</font>";
    }
    include_once('../footer.php');
?>
