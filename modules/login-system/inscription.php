<?php 
require_once('../header.php'); 
require_once('../../config/db.php');

if(isset($_SESSION['id']) && $_SESSION['id'] > 0) {
    header("Location: ../../index.php");
}
else if(isset($_POST['inscriptionButton'])) {

    $pseudo = trim(htmlspecialchars($_POST['pseudoForm']));
    $mail = trim(htmlspecialchars($_POST['emailForm']));
    $mail2 = trim(htmlspecialchars($_POST['mailConfirmForm']));
    $mdp = trim(htmlspecialchars($_POST['passForm']));
    $mdp2 = trim(htmlspecialchars($_POST['passConfirmForm']));
    $mdpHash = password_hash($mdp, PASSWORD_DEFAULT);
    $mdpVerify = password_verify($mdp2, $mdpHash);

    if(!empty($pseudo) && !empty($mail) && !empty($mail2) && !empty($mdp) && !empty($mdp2)) {
        if(strlen($pseudo) <= 30) {
            $reqpseudo = $bdd -> prepare("SELECT pseudo FROM membres WHERE pseudo = ?");
            $reqpseudo -> execute(array($pseudo));
            $pseudoExist = $reqpseudo -> rowCount();
            if($pseudoExist == 0) {
                if($mail = $mail2) {
                    if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                        $reqmail = $bdd -> prepare("SELECT mail FROM membres WHERE mail = ?");
                        $reqmail -> execute(array($mail));
                        $mailExist = $reqmail -> rowCount();
                        if($mailExist == 0) {
                            if($mdpVerify == 1) {
                                $tokenpass = bin2hex(openssl_random_pseudo_bytes(32));
                                $insertmbr = $bdd -> prepare("INSERT INTO membres (pseudo, mail, tokenpass, motdepasse) VALUES (?, ?, ?, ?)");
                                $insertmbr -> execute(array($pseudo, $mail, $tokenpass, $mdpHash));
                                $success = "Votre compte a bien été créé !";
                            } else {
                                $erreur = "Vous mots de passe ne correspondent pas";
                            }
                        } else {
                            $erreur = "Adresse mail déjà attribuée";
                        }
                    } else {
                        $erreur = "Votre mail n'est pas valide";
                    }
                } else {
                    $erreur = "Vos deux mail ne correspondent pas";
                }
            } else {
                $erreur = "Ce pseudo est déjà utilisé";
            }         
        } else {
            $erreur = "Votre pseudo ne doit pasdépasser 30 caractères";
        }
    } else {
        $erreur = "Tous les champs doivent être complétés";
    }


}

?>

<div class="container">
    <form id="formInscription" method="POST">
        <div class="form-group">
            <label for="pseudoForm">Votre Pseudo</label>
            <input type="text" class="form-control" name="pseudoForm" placeholder="Votre pseudo">
        </div>
        <div class="form-group">
            <label for="emailForm">Votre e-mail</label>
            <input type="email" class="form-control" name="emailForm" placeholder="Votre mail">
        </div>
        <div class="form-group">
            <label for="mailConfirmForm">Confirmez le mail</label>
            <input type="email" class="form-control" name="mailConfirmForm" placeholder="Confirmez votre mail">
        </div>
        <div class="form-group">
            <label for="passForm">Votre mot de passe</label>
            <input type="password" class="form-control" name="passForm" placeholder="Votre mot de passe">
        </div>
        <div class="form-group">
            <label for="passConfirmForm">Confirmez le mdp</label>
            <input type="password" class="form-control" name="passConfirmForm" placeholder="Confirmez votre mdp">
        </div>
        <button type="submit" name="inscriptionButton" class="btn btn-primary">S'inscrire</button>
    </form>
</div>

<?php
    if(isset($erreur)) {
    echo '<font color="red">'.$erreur."</font>";
    }
    if(isset($success)) {
    echo '<font color="green">'.$success."</font>";
    }
?>
<p>Déjà inscrit ? <a href="connexion.php">Se connecter</a></p>
<?php require_once('../footer.php'); ?>
