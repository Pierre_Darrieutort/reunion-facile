<?php

include_once("modules/header.php");

if(!isset($_SESSION['id'])) {
    header("Location: modules/login-system/connexion.php");
}

?>
<div class="container text-center">
    <div class="row p-3">
        <a href="modules/login-system/deconnexion.php" class="w-100">Se déconnecter</a>
    </div>
</div>
<div class="container text-center">
    <div class="row p-3">
        <p>Connecté en tant que : <?= $_SESSION['pseudo'] ?></p>
    </div>
</div>
<?php include_once("modules/footer.php") ?>
